#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>

#define NUM_THREADS 3
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

char *Itop = "Iter-OPT";
char *Itunop = "Iter-UNOPT";
char *Recop = "Rec-OPT";
char *Recunop = "Rec-UNOPT";

int N_Threads = 4;

long size;
char *a;
long times;
unsigned long lowest = 4294967295;
char *best_version;

void *rec(char *a, int pos);
void *recOP(char *a, int pos, int len);

void *testITOP();
void *testITUNOP();
void *testRECOP();
void *testRECUNOP();

void *looping();

int main(void)
{
    best_version = (char *)malloc(sizeof(char) * 10);

    for (size = 2048; size >= 2048; size /= 2)
    {
        printf("LEN STR %d\n", size);
        for (times = 1024; times >= 1024; times /= 2)
        {
            a = (char *)malloc(sizeof(char) * size);
            while (strlen(a) < size)
            {
                strcat(a, "a");
            }

            printf("\t TIMES %d\n", times);
            void *funcs[N_Threads];
            funcs[0] = testITUNOP;
            funcs[1] = testITOP;
            funcs[2] = testRECUNOP;
            funcs[3] = testRECOP;

            pthread_t threads[N_Threads];
            int thread_args[N_Threads];
            int res;

            for (int i = 0; i < N_Threads; i++)
            {
                thread_args[i] = i;
                res = pthread_create(&threads[i], NULL, funcs[i], &thread_args[i]);
                assert(!res);
            }

            for (int i = 0; i < N_Threads; i++)
            {
                res = pthread_join(threads[i], NULL);
                assert(!res);
            }
            free(a);
            printf("\nLowest - %s -  %lu\n\n", best_version, lowest);
        }
    }
    free(best_version);
    return 0;
}

void *rec(char *a, int pos)
{
    if (pos < strlen(a))
    {
        a[pos] = 'b';
        rec(a, pos + 1);
    }
}
void *recOP(char *a, int pos, int len)
{
    if (pos < len)
    {
        a[pos] = 'b';
        recOP(a, pos + 1, len);
    }
}

void *testITOP()
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    for (int i = 0; i < times; i++)
    {
        for (int k = 0, j = strlen(a); k < j; k++)
        {
            a[k] = 'b';
        }
    }
    gettimeofday(&stop, NULL);
    printf("\t\ttook %lu us ITER optimized\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    if (tmp < lowest)
    {
        lowest = MIN(tmp, lowest);
        strcpy(best_version, Itop);
    }
}
void *testITUNOP(void *arguments)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    for (int i = 0; i < times; i++)
    {
        for (int k = 0; k < strlen(a); k++)
        {
            a[k] = 'b';
        }
    }
    gettimeofday(&stop, NULL);
    printf("\t\ttook %lu us ITER unoptimized\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    if (tmp < lowest)
    {
        lowest = MIN(tmp, lowest);
        strcpy(best_version, Itunop);
    }
}
void *testRECOP()
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    for (int i = 0; i < times; i++)
    {
        recOP(a, 0, strlen(a));
    }
    gettimeofday(&stop, NULL);
    printf("\t\ttook %lu us Rec optimized\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    if (tmp < lowest)
    {
        lowest = MIN(tmp, lowest);
        strcpy(best_version, Recop);
    }
}
void *testRECUNOP()
{

    struct timeval stop, start;
    gettimeofday(&start, NULL);
    for (int i = 0; i < times; i++)
    {
        rec(a, 0);
    }
    gettimeofday(&stop, NULL);
    printf("\t\ttook %lu us REC unoptimized\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    if (tmp < lowest)
    {
        lowest = MIN(tmp, lowest);
        strcpy(best_version, Recunop);
    }
}