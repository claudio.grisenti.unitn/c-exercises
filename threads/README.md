# Threads

### compile*
This bash files contain commands to compile each .c files contained in the folder

### [arrays.c](/threads/arrays.c)
This source code contains a program which evaluates what is the best number of threads to modify an array


### [strings.c](/threads/strings.c)
This source code contains a program which evaluates the difference in time when working with strings with these 2 different approaches.

1. `for (int i = 0; i < strlen(string);i++)` <br/> <br/>
2. `for (int i = 0, strlen = strlen(string) ; i < strlen;i++)` <br/><br/>

This program calculate the time needed both in a iterative way and a recursive way.

### [alg_complexity.c](/threads/alg_complexity.c)
This source code contains a program which demonstrates the difference in time for common algorithms complexcity.