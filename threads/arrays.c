#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>

void *lvlupArray(void *data);
void setThread();
int checkArrayConsistency(int *array, int n);
void setArray(int *array, int n);

struct lvlupArrayStruct
{
    int *array;
    int n;
    int pos;
    int offset;
};

int main()
{
    printf("\n\n");

    setThread();

    printf("\n\n");
    return 0;
}

void setThread()
{
    int fastetThread;
    int maxNthreads = 64;
    unsigned long time = -1;
    int size = 1024 * 1024;
    struct timeval stop, start;
    for (int tries = 0; tries < 1; tries++)
    {
        for (int nThreads = 1; nThreads <= maxNthreads; nThreads++)
        {
            int *array = (int *)malloc(sizeof(int) * size);
            //printf("Number of Threads -> %d\n", nThreads);
            struct lvlupArrayStruct data[nThreads];

            gettimeofday(&start, NULL);
            for (int i = 0; i < nThreads; i++)
            {
                data[i].n = size;
                data[i].array = array;
                data[i].offset = nThreads;
                data[i].pos = i;
            }
            setArray(array, size);

            pthread_t threads[nThreads];
            int res;

            for (int i = 0; i < nThreads; i++)
            {
                res = pthread_create(&threads[i], NULL, lvlupArray, &data[i]);
                assert(!res);
            }

            for (int i = 0; i < nThreads; i++)
            {
                res = pthread_join(threads[i], NULL);
                assert(!res);
            }

            gettimeofday(&stop, NULL);
            unsigned long currenttime = (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;
            printf("\t\ttook %lu us with %d thread(s)\n", currenttime, nThreads);
            if (currenttime < time)
            {
                fastetThread = nThreads;
                time = currenttime;
            }
            if (checkArrayConsistency(array, size))
            {
                printf("Array not leveled up correctly\n");
            }
            free(array);
        }
    }

    printf("Fastet number of thread %d with %lu us", fastetThread, time);
}

void *lvlupArray(void *data)
{
    struct lvlupArrayStruct *datatmp = ((struct lvlupArrayStruct *)data);
    for (int i = datatmp->pos, n = datatmp->n, offset = datatmp->offset; i < n; i += offset)
    {
        datatmp->array[i] += 1;
    }
}

int checkArrayConsistency(int *array, int n)
{
    int result = 1;
    for (int i = 0; result && (i < n); i++)
    {
        result = (array[i] != 2);
    }
    return result;
}
void setArray(int *array, int n)
{
    for (int i = 0; i < n; i++)
    {
        array[i] = 1;
    }
}