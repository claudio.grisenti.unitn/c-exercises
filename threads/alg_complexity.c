#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/syscall.h>

void *mylog(void *pos);
void *sublinear(void *pos);
void *linear(void *pos);
void *loglinear(void *pos);
void *quadratic(void *pos);
void *cubic(void *pos);
void *exponential(void *pos);
void myexp(int n);
void takeTime();

int main()
{

    int size, func_numb = 7;
    void *funcs[func_numb];
    funcs[0] = mylog;
    funcs[1] = sublinear;
    funcs[2] = linear;
    funcs[3] = loglinear;
    funcs[4] = quadratic;
    funcs[5] = cubic;
    funcs[6] = exponential;

    pthread_t threads[func_numb];
    int res;
    for (size = 4; size <= 64; size *= 2)
    {
        printf("Input Size - %d\n", size);
        for (int i = 0; i < func_numb; i++)
        {
            res = pthread_create(&threads[i], NULL, funcs[i], (void *)&size);
            assert(!res);
        }

        for (int i = 0; i < func_numb; i++)
        {
            res = pthread_join(threads[i], NULL);
            assert(!res);
        }
        sleep(1);
    }
    return 0;
}

void *mylog(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    int i = *((int *)pos);
    while (i > 1)
    {
        takeTime();
        i /= 2;
    }

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tLog - took %lu us \n", tmp);
}
void *sublinear(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    int i = *((int *)pos);
    while (i > 1)
    {
        takeTime();
        i = pow(i, 0.5);
    }

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tSubLinear - took %lu us \n", tmp);
}
void *linear(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    int i = *((int *)pos);
    while (i > 0)
    {
        takeTime();
        i--;
    }

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tLinear - took %lu us \n", tmp);
}

void *loglinear(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    for (int i = *((int *)pos); i > 0; i--)
    {
        int j = i;
        while (j > 1)
        {
            takeTime();
            j /= 2;
        }
    }

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tLogLinear - took %lu us \n", tmp);
}
void *quadratic(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    for (int i = *((int *)pos); i > 0; i--)
    {
        for (int j = i; j > 0; j--)
            takeTime();
    }

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tQuadratic - took %lu us \n", tmp);
}
void *cubic(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    for (int i = *((int *)pos); i > 0; i--)
    {
        for (int j = i; j > 0; j--)
        {
            for (int k = j; k > 0; k--)
                takeTime();
        }
    }

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tCubic - took %lu us \n", tmp);
}
void *exponential(void *pos)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    myexp(*((int *)pos));

    gettimeofday(&stop, NULL);
    unsigned long tmp = ((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
    printf("\t\tExp - took %lu us \n", tmp);
}
void myexp(int n)
{
    //printf("%d\n",n);
     if (n > 1)
    {   
        takeTime();
        myexp(n - 1);
        myexp(n - 2);
    }
    return;
}

void takeTime()
{
    int times = 1024;
    for (int i = 0; i < times; i++)
    {
        int a = 0;
        a++;
    };
}